function ResizeBanner(){
	var headerHeight = $("#header-block").height();
	var footerHeight = $("#header-block").height();
	var heightScreen = $(window).height();
	var heightTotal = heightScreen - (headerHeight + footerHeight);

	$(".resize").css({'height': heightTotal});


}

function ResizeContent(){
	var headerHeight = $("#header-block").height();
	var footerHeight = $("#header-block").height();
	var heightScreen = $(window).height();
	var heightTotal = heightScreen - (headerHeight + footerHeight);

	$(".resizeContent").css({'height': heightTotal});
}

function openMenu(){
	$("#btnOpenMenu").on("click", function(){
		if($(".menu").hasClass("open")){
			$(".menu").removeClass("open");
		}
		else{
			$(".menu").addClass("open");
		}
	});
}

function menuResponsive(){
	$(window).scroll(function(){
		if($(".menu").hasClass("open")){
			$(".menu").removeClass("open");
		}
		var top = ($(window).scrollTop());
        if(top >= 80){
			$(".header ").addClass("header-responsive");
		}
		else{
			$(".header ").removeClass("header-responsive");
		}
	});

}