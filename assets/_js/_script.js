$(window).ready(function(){
	ResizeBanner();
	ResizeContent();
	openMenu();
	menuResponsive();
	setClassActive();
});

function setClassActive(){
	var selector = '.menu-links li';
	$(selector).on("click", function(e) {
    	$(selector).removeClass('active');
    	$(this).addClass('active');
    });
}
