<?php include('header.php'); ?>
	<div class="default col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
            $post = get_post('10'); 
            $postIdCont = $post->ID;
            $banner_rest = get_field('background_info', $postIdCont);
            $titulo_info = get_field('titulo_info', $postIdCont);
            $conteudo_info = get_field('conteudo_info', $postIdCont);
            $titulo_contato = get_field('titulo_contato', $postIdCont);
            $titulo_reserva = get_field('reserva_titulo_contato', $postIdCont);
        ?>
		<div class="row default-container">
			<div class="block-left cover resize col-lg-8 col-xs-12 col-md-8 col-sm-12" style="background-image: url(<?php echo $banner_rest; ?>);">
			</div>
			<div class="block-right col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="block-right--title">
					<h2><?php echo $titulo_info; ?></h2>
				</div>
				<div class="block-right--text">
					<?php echo $conteudo_info; ?>
				</div>
				<div class="block-right--content">
					<div class="content-title">
						<h3><?php echo $titulo_contato; ?></h3>
					</div>
					<div class="content-list">
						<div class="row ">
						<?php
	                        while ( have_rows('horarios_semanais_contato') ) : the_row();
	                            $dia_semana_contato = get_sub_field('dia_da_semana_contato', $postIdCont);
	                            $horario_almoco = get_sub_field('horario_de_refeicao_contato_almoco', $postIdCont);
	                            $horario_jantar = get_sub_field('horario_de_refeicao_contato_jantar', $postIdCont);
	                    ?>
							<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4">
								<ul>
									<li><?php echo $dia_semana_contato; ?></li>
									<li><?php echo $horario_almoco; ?></li>
									<li><?php echo $horario_jantar; ?></li>
								</ul>
							</div>
						<?php endwhile; ?>
						</div>
					</div>
				</div>
				<div class="block-right--content">
				 	<?php
                		$post = get_post('20'); 
            		    $postIdEnd = $post->ID;
            		    $endereco_contato = get_field('endereco_menu', $postIdEnd);
        		    ?>
					<div class="content-title">
						<h3>VENHA NOS VISITAR</h3>
					</div>
					<div class="content-text">
						<?php echo $endereco_contato; ?>
					</div>
				</div>
				<div class="block-right--content">
					<div class="content-title">
						<h3><?php echo $titulo_reserva; ?></h3>
					</div>
					<div class="content-list">
						<div class="row ">
							<?php
								$post = get_post('10'); 
							    $postIdReserv = $post->ID;
		                        while ( have_rows('telefones_para_reserva_contato') ) : the_row();
		                            $tipo_telefone_reserva = get_sub_field('tipo_de_telefone_reserva', $postIdReserv);
		                            $numero_telefone_reserva = get_sub_field('numero_telefone_reserva', $postIdReserv);
		                    ?>
								<div class="col-lg-4 col-xs-12 col-md-4 col-sm-4">
									<ul>
										<li><?php echo $tipo_telefone_reserva; ?></li>
										<li><?php echo $numero_telefone_reserva; ?></li>
									</ul>
								</div>
							<?php endwhile; ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>






<?php include('footer.php'); ?>