<?php include('header.php'); ?>
	<div class="default default-space col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
    		$post = get_post('15'); 
		    $postIdChef = $post->ID;
		    $titulo_chef = get_field('titulo_chef', $postIdChef);
		    $conteudo_chef = get_field('conteudo_chef', $postIdChef);
	    ?>
		<div class="row default-container">
			<div class="block-left pages resizeContent chef col-lg-8 col-xs-12 col-md-8 col-sm-12">
				<div class="row chef-page">
					<?php 
					    $args = array(
					        'post_type' => 'chefs',
					        'post_status' => 'publish',
					    );
					    $my_posts = new WP_Query( $args );
				        while ( $my_posts->have_posts() ) : $my_posts->the_post();
			            	$post 		= get_post();        
							$postIdChefs 	= $post->ID; 
							$imagem_chef 	= get_field('foto_chef', $postIdChefs); 		
	             	?>
						<div class="chef-item col-lg-4 col-xs-12 col-md-4 col-sm-4">
							<div class="chef-item-block">
								<div class="block-image">
									<img src="<?php echo $imagem_chef; ?>">
								</div>
								<div class="block-title">
									<h2><?php the_title(); ?></h2>
								</div>
								<div class="block-text">
									<?php the_content(); ?>
								</div>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
			<div class="block-right pages resizeContent col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="block-right--title">
					<h2><?php echo $titulo_chef; ?></h2>
				</div>
				<div class="block-right--text">
					<?php echo $conteudo_chef; ?>
				</div>
			</div>
		</div>
	</div>






<?php include('footer.php'); ?>