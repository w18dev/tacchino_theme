<?php include('header.php'); ?>
	<div class="default default-space col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
            $post = get_post('17'); 
            $postIdMenu = $post->ID;
            $titulo_menu = get_field('titulo_menu', $postIdMenu);
            $conteudo_menu = get_field('conteudo_menu', $postIdMenu);
        ?>
		<div class="row default-container">
			<div class="block-left pages resizeContent menu col-lg-8 col-xs-12 col-md-8 col-sm-12">
				<div class="row menu-page">
					<?php 
					    $args = array(
					        'post_type' => 'menu_pratos',
					        'post_status' => 'publish',
					    );
					    $my_posts = new WP_Query( $args );
				        while ( $my_posts->have_posts() ) : $my_posts->the_post();
			            	$post 		= get_post();        
							$postIdMenuCard 	= $post->ID; 
							$imagem_menu  = get_field('imagem_menu', $postIdMenuCard); 		
							$pdf_link  = get_field('pdf_menu', $postIdMenuCard); 		
	             	?>
						<div class="menu-item col-lg-4 col-xs-12 col-md-4 col-sm-4">
							<div class="menu-item-block">
								<div class="block-image">
									<img src="<?php echo $imagem_menu; ?>">
								</div>
								<div class="block-title">
									<h2><?php the_title(); ?></h2>
								</div>
								<div class="block-text">
									<?php the_content(); ?>
								</div>
								<div class="block-view">
									<a href="<?php echo $pdf_link; ?>" target="_blank"><i class="fa fa-external-link"></i><span>ver pdf.</span></a>
								</div>
							</div>
						</div>
					<?php endwhile; wp_reset_query(); ?>
				</div>
			</div>
			<div class="block-right pages resizeContent col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="block-right--title">
					<h2><?php echo $titulo_menu; ?></h2>
				</div>
				<div class="block-right--text">
					<?php echo $conteudo_menu; ?>
				</div>
			</div>
		</div>
	</div>






<?php include('footer.php'); ?>