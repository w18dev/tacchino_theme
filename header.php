<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" href="<?php bloginfo('template_directory'); ?>/css/champs.min.css">
        <link rel="icon" type="image/png" sizes="96x96" href="<?php bloginfo('template_directory'); ?>/images/favicon/favicon-96x96.png">
        <link href="https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700" rel="stylesheet">
        <!--[if lt IE 8]>
            <script type="text/javascript" src="js/html5shiv.min.js"></script>
        <![endif]-->
        <script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/modernizr-2.6.2.min.js"></script>
    </head>
<body>
   <div class="header col-lg-12 col-xs-12 col-sm-12 col-md-12" id="header-block">
       <div class="row header-container">
            <?php
                $post = get_post('20'); 
                $postIdHeader = $post->ID;
                $logo_header = get_field('logo_header', $postIdHeader);
                $icone_menu_header = get_field('icone_menu_header', $postIdHeader);
                $endereco_menu = get_field('endereco_menu', $postIdHeader);
                $telefone_menu = get_field('telefone_menu', $postIdHeader);
            ?>
           <div class="header-container-logo text-left col-lg-6 col-md-6 col-xs-6">
                <h1><a href="<?php echo get_site_url(); ?>"><img src="<?php echo $logo_header; ?>"></a></h1>
           </div>
           <div class="header-container-menu text-right col-lg-6 col-md-6 col-xs-6">
                <button type="button" id="btnOpenMenu"><img src="<?php echo $icone_menu_header; ?>"></button>
                <div class="menu resizeContent">
                    <div class="menu-list text-left"> 
                        <ul class="menu-links avenir-font">
                            <li><a href="<?php bloginfo('url'); ?>/restaurante">RESTAURANTE</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/chef">O CHEF</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/menu">MENU</a></li>
                            <li><a href="<?php bloginfo('url'); ?>/almoco">MENU DE ALMOÇO</a></li>
                            <li>
                                <span>EVENTOS</span>
                                <ul class="menu-sub">
                                    <li><a href="<?php bloginfo('url'); ?>/eventos-corporativos">- CORPORATIVOS</a></li>
                                    <li><a href="<?php bloginfo('url'); ?>/eventos-particulares">- PARTICULARES</a></li>
                                </ul>
                            </li>
                            <li><a href="<?php bloginfo('url'); ?>/contato">CONTATO</a></li>
                        </ul>
                    </div>
                    <div class="menu-list-info">
                        <div class="info-end text-center avenir-font">
                            <?php echo $endereco_menu; ?>
                        </div>
                        <div class="info-tel text-center avenir-font">
                            <p>Tel: <?php echo $telefone_menu; ?></p>
                        </div>
                        <div class="info-social text-center">
                            <ul>
                                <?php
                                    $post = get_post('20'); 
                                    $postIdSocial = $post->ID;
                                    // check if the repeater field has rows of data
                                        // loop through the rows of data
                                    while ( have_rows('redes_sociais_carousel') ) : the_row();
                                         $icone_rede_social = get_sub_field('icone_da_rede_social_menu', $postIdSocial);
                                         $link_rede_social = get_sub_field('link_da_rede_social_menu', $postIdSocial);
                                ?>
                                    <li><a href="<?php echo $link_rede_social; ?>" target="_blank"><i class="<?php echo $icone_rede_social; ?>"></i></a></li>
                                <?php endwhile; ?>
                            </ul>
                        </div>
                    </div>
                </div>
           </div>
       </div>
   </div>