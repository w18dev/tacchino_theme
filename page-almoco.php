<?php include('header.php'); ?>
	<div class="default col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
            $post = get_post('6'); 
            $postIdAlmoco = $post->ID;
            $banner_rest = get_field('background_info', $postIdAlmoco);
            $titulo_info = get_field('titulo_info', $postIdAlmoco);
            $conteudo_info = get_field('conteudo_info', $postIdAlmoco);
            $titulo_almoco = get_field('titulo_almoco', $postIdAlmoco);
            $valor_almoco = get_field('valor_almoco', $postIdAlmoco);
        ?>
		<div class="row default-container">
			<div class="block-menu resizeContent pages col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="container-fluid">
					<div class="row block-menu--title">
						<div class="pull-left">
							<h2><?php echo $titulo_almoco; ?></h2>
						</div>
						<div class="pull-right">
							<span><?php echo $valor_almoco; ?></span>
						</div>
					</div>
					<?php
                        while ( have_rows('carousel_refeicoes_almoco') ) : the_row();
                            $titulo_lista = get_sub_field('titulo_lista_refeicao', $postIdAlmoco);
                    ?>
						<div class="row block-menu--list">
							<div class="list-menu">
								<ul>
									<li>
										<div class="list-title">
											<h3><?php echo $titulo_lista; ?></h3>
										</div>
									</li>
									<?php
				                        while ( have_rows('refeicoes_sub_carousel') ) : the_row();
				                            $titulo_refeicao = get_sub_field('nome_refeicao_sub', $postIdAlmoco);
				                            $descricao_refeicao = get_sub_field('descricao_refeicao_sub', $postIdAlmoco);
				                    ?>
										<li>
											<div class="list-menu--title">
												<span><?php echo $titulo_refeicao; ?></span>
											</div>
											<div class="list-menu--desc">
												<span><?php echo $descricao_refeicao; ?></span>
											</div>
										</li>
									<?php endwhile; ?>		
								</ul>
							</div>
						</div>
					<?php endwhile; ?>
				</div>
			</div>
			<div class="block-left cover resize col-lg-4 col-xs-12 col-md-4 col-sm-12" style="background-image: url(<?php echo $banner_rest; ?>);">
			</div>
			<div class="block-right pages resizeContent col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="block-right--title">
					<h2><?php echo $titulo_info; ?></h2>
				</div>
				<div class="block-right--text">
					<?php echo $conteudo_info; ?>
				</div>
			</div>
		</div>
	</div>






<?php include('footer.php'); ?>