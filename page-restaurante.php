<?php include('header.php'); ?>
	<div class="default col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
            $post = get_post('8'); 
            $postIdRest = $post->ID;
            $banner_rest = get_field('background_info', $postIdRest);
            $titulo_info = get_field('titulo_info', $postIdRest);
            $conteudo_info = get_field('conteudo_info', $postIdRest);
        ?>
		<div class="row default-container">
			<div class="block-left cover resize col-lg-8 col-xs-12 col-md-8 col-sm-12" style="background-image: url('<?php echo $banner_rest; ?>')">
			</div>
			<div class="block-right pages resizeContent col-lg-4 col-xs-12 col-md-4 col-sm-12">
				<div class="block-right--title">
					<h2><?php echo $titulo_info; ?></h2>
				</div>
				<div class="block-right--text">
					<?php echo $conteudo_info; ?>
				</div>
			</div>
		</div>
	</div>






<?php include('footer.php'); ?>