<?php include('header.php'); ?>
	<div class="home col-lg-12 col-xs-12 col-sm-12 col-md-12">
		<?php
            $post = get_post('20'); 
            $postIdHome = $post->ID;
            $banner_home = get_field('banner_home', $postIdHome);
            $logo_banner_home = get_field('logo_banner_home', $postIdHome);
            $titulo_home = get_field('titulo_home', $postIdHome);
            $subtitulo_home = get_field('subtitulo_home', $postIdHome);
        ?>
		<div class="home-container resize" style="background-image: url('<?php echo $banner_home; ?>')">
			<div class="home-container-block">
				<img src="<?php echo $logo_banner_home; ?>">
				<div class="block-container">
					<div class="block-container--title">
						<h2><?php echo $titulo_home; ?></h2>
					</div>
					<div class="block-container--text">
						<p><?php echo $subtitulo_home; ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>




<?php include('footer.php'); ?>